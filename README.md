# Motivado

This is the source code of [todo.miloszdragan.pl](http://todo.miloszdragan.pl)

Motivado is a Laravel (PHP) webapp.
It is a simple to-do list that requires you to define why you want to complete your tasks,
thus improving your motivation.
